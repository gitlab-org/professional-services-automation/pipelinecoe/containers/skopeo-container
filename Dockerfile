FROM registry.gitlab.com/gitlab-org/professional-services-automation/pipelinecoe/containers/container-base:latest

# Add Operations Here
RUN dnf -y install skopeo

# Leave this so it can't be executed normally.
CMD ["echo", "This is a 'Purpose-Built Container', It is not meant to be ran this way. Please review the documentation on usage."]
